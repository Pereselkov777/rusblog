<?php

namespace app\modules\blog;

/**
 * blog module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\blog\controllers';

    public $adminRoles = ['administrator', 'admin', 'superadmin'];
    public $layout_frontend = '//main';
    public $layout_admin = '//admin';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

       $this->layout = $this->layout_admin;
        // custom initialization code goes here
    }
}
