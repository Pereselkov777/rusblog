<?php

namespace app\modules\blog\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "{{%blog_tags}}".
 *
 * @property int $id ID
 * @property string $slug Алиас
 * @property string $title Заголовок
 * @property string $created_at Создано
 * @property int $weight Вес
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%blog_tag}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slug', 'title'], 'required'],
            [['created_at'], 'safe'],
            [['weight'], 'integer'],
            [['slug', 'title'], 'string', 'max' => 32],
            [['slug'], 'unique'],//?????
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'slug' => Yii::t('app', 'Алиас'),
            'title' => Yii::t('app', 'Заголовок'),
            'created_at' => Yii::t('app', 'Создано'),
            'weight' => Yii::t('app', 'Вес'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                // 'slugAttribute' => 'slug',
            ],
     /*       'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ]*/
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                // Да это новая запись (insert)
                $this->created_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }

    /**
     * Returns tag names and their corresponding weights.
     * Only the tags with the top weights will be returned.
     * @param integer the maximum number of tags that should be returned
     * @return array weights indexed by tag names.
     */
    public static function findTagWeights($limit=20)
    {
        $models = self::find()->orderBy(['weight' => SORT_DESC])->limit($limit)->all();

        $total = 0;
        foreach($models as $model)
            $total+=$model->weight;

        $tags=[];
        if($total > 0)
        {
            foreach($models as $model)
                $tags[$model->slug]= ['weight' => 12+(int)(16*$model->weight/($total+10)), 'title' => $model->title];
            ksort($tags);
        }
        return $tags;
    }

}
