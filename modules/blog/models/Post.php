<?php

namespace app\modules\blog\models;

use Yii;
use dosamigos\taggable\Taggable;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "{{%post}}".
 *
 * @property int $id id
 * @property string $text �����
 * @property int $user_id id �����
 * @property string $created_at �������
 * * @property string $slug �����
 *  @property PostTag[] $tags ����
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    
    //public $slug;
    
    public static function tableName()
    {
        return '{{%post}}';
    }



    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                //'slugAttribute' => 'slug',
            ],
            'tagNames' => [
                'class' => Taggable::className(),
                'attribute' => 'tagNames',
                'name' => 'title',
                'frequency' => 'weight'
            ]
            // for different configurations, please see the code
            // we have created tables and relationship in order to
            // use defaults settings
        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = date('Y-m-d H:i:s');
                $this->user_id = Yii::$app->user->id;
            }
            return true;
        }
    }

    public function getPhoto($id)
    {

        $q = 'SELECT * FROM photo WHERE photo.post_id =' . $id . '';
        $photo = Yii::$app->db->createCommand($q)->queryOne();
        /*var_dump($id);
        var_dump($q);
        var_dump($photo);
        exit();*/
        return $photo;

    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'title','slug'], 'required'],
            [['user_id'], 'integer'],
            //[['title'], 'unique'],
            //[['text'], 'text'],
            [['created_at'], 'safe'],
            [['title', 'slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['tagNames'], 'safe'],
        ];
    }


    /**
     *  @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(PostTag::className(), ['id' => 'tag_id'])->viaTable('{{%post_via_tag}}', ['post_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'text' => Yii::t('app', 'Text'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Alias'),
        ];
    }
}