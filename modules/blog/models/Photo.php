<?php

namespace app\modules\blog\models;

use Yii;

/**
 * This is the model class for table "{{%photo}}".
 *
 * @property int $id
 * @property int $post_id Пользователь
 * @property string $title Название
 * @property string $img_full Полное изображение
 * @property string $img_prev Малое изображение
 * @property int $status Статус
 * @property string $create_time Создано
 * @property string $update_time Обновлено
 */
class Photo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%photo}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_id', 'title', 'img_full', 'img_prev', 'status', 'create_time', 'update_time'], 'required'],
            [['post_id', 'status'], 'integer'],
            [['create_time', 'update_time'], 'safe'],
            [['title', 'img_full', 'img_prev'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_id' => Yii::t('app', 'Пользователь'),
            'title' => Yii::t('app', 'Название'),
            'img_full' => Yii::t('app', 'Полное изображение'),
            'img_prev' => Yii::t('app', 'Малое изображение'),
            'status' => Yii::t('app', 'Статус'),
            'create_time' => Yii::t('app', 'Создано'),
            'update_time' => Yii::t('app', 'Обновлено'),
        ];
    }
}
