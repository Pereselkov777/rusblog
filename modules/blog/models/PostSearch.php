<?php

namespace app\modules\blog\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\blog\models\Post;


/**
 * PostSearch represents the model behind the search form of `app\modules\blog\models\Post`.
 */
class PostSearch extends Post
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['text', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Post::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);

        $this->load($params);
        
       // echo('searchMOdel');
        //var_dump($params);exit();
        
       if($params && $params['PostSearch']['tag_id']){
            //echo('HERE QUERY');
            //var_dump($params);
            //exit();
            $query->innerJoin('{{%post_via_tag}} as rs','rs.post_id = {{%post}}.id')->distinct();
            $query->innerJoin('{{%post_tag}} as s','s.id = rs.tag_id')->distinct();
            $query->andFilterWhere(['s.id' => $params['PostSearch']['tag_id']]);
            //var_dump($query->prepare(\Yii::$app->db->queryBuilder)->createCommand()->rawSql);
           // exit();
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
