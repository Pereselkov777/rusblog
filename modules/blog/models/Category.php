<?php

namespace app\modules\blog\models;

use Yii;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%blog_categories}}".
 *
 * @property int $id ID
 * @property int $parent_id Родитель
 * @property string $slug Псевдоним
 * @property string $title Заголовок
 * @property string $created_at Создано

 * @property Category $parent Родитель
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%blog_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer'],
            [['slug','title'], 'required'],
            [['created_at'], 'safe'],
            [['slug','title'], 'string', 'max' => 128],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Родитель'),
            'slug' => Yii::t('app', 'Псевдоним'),
            'title' => Yii::t('app', 'Заголовок'),
            'created_at' => Yii::t('app', 'Создано'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                // Да это новая запись (insert)
                $this->created_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }

    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    public function getParentName()
    {
        return $this->parent ? $this->parent->title : null;
    }
    public static function rubrics_menu($row_menu,$level, &$result_menu)
    {
        $id=$row_menu['id'];
        $result_menu.='<li class="list-group-item">'.Html::a($row_menu['title'], ['category', 'slug' => $row_menu['slug']],[
            'style' =>   'padding-left: '.($level*15).'px;',
            ]);
        $result_menu.='</li>';
        $res=self::find()->where('parent_id = :par',[':par'=>$id])->all();
        foreach( $res as $row)
        {
//            $result_menu.='<ul class="list-group">';
            self::rubrics_menu($row, $level+1, $result_menu);
//            $result_menu.='</ul>';
        }
//        $result_menu.='</li>';
    }

    public static function getAllCategoryList($selfId = false)
    {
        $where = ($selfId) ? 'where id <> '.$selfId : '';
        $category = self::findBySql('select * from {{%blog_category}} '.$where.' order by title' )->all();
        return  ArrayHelper::map($category, 'id', 'title');
    }

}
