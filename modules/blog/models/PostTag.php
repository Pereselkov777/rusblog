<?php

namespace app\modules\blog\models;
use yii\behaviors\SluggableBehavior;

use Yii;

/**
 * This is the model class for table "{{%post_tag}}".
 *
 * @property int $id id
 * @property string $slug Алиас
 * @property string $title Название
 * @property int $weight Вес
 * @property string|null $created_at Создано
 */
class PostTag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%post_tag}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slug', 'title'], 'required'],
            [['weight'], 'integer'],
            [['created_at'], 'safe'],
            [['slug', 'title'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['title'], 'unique'],
        ];
    }

    public function behaviors()

    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                // 'slugAttribute' => 'slug',
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                // Да это новая запись (insert)
                $this->created_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'slug' => Yii::t('app', 'Slug'),
            'title' => Yii::t('app', 'Title'),
            'weight' => Yii::t('app', 'weight'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    
    /**
     * Returns tag names and their corresponding weights.
     * Only the tags with the top weights will be returned.
     * @param integer the maximum number of tags that should be returned
     * @return array weights indexed by tag names.
     */
    public static function findTagWeights($limit=20)
    {
        $models = self::find()->orderBy(['weight' => SORT_DESC])->limit($limit)->all();

        
        
        $total = 0;
        foreach($models as $model)
            //var_dump($model->weight);exit();
            $total+=$model->weight;

        $tags=[];
        if($total > 0)
        {   
           
            foreach($models as $model)
                $tags[$model->slug]= ['weight' => 12+(int)(16*$model->weight/($total+10)), 'title' => $model->title];
            ksort($tags);
        }
        //var_dump($tags);exit();
        return $tags;
    }

}
