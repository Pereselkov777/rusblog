<?php

namespace app\modules\blog\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\blog\models\Article;

/**
 * ArticleSearch represents the model behind the search form of `app\modules\blog\models\Article`.
 */
class ArticleSearch extends Article
{
    public $tag_id;

    public $start_time;
    public $end_time;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'author_id', 'category_id', 'is_commentable', 'is_enabled', 'tag_id'], 'integer'],
            [['title','meta_title', 'slug', 'annotation', 'text', 'keywords', 'category'
                , 'description', 'created_at', 'updated_at', 'start_time', 'end_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find();

        //var_dump($query->prepare(\Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        //exit();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 2,
                
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'author_id' => $this->author_id,
            'category_id' => $this->category_id,
            'is_commentable' => $this->is_commentable,
            'is_enabled' => $this->is_enabled,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'annotation', $this->annotation])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'keywords', $this->keywords])
            ->andFilterWhere(['like', 'description', $this->description]);

        if ($this->tag_id){
//            $ids = explode(',',$this->skill_ids);
            $query->innerJoin('{{%blog_article_via_tag}} as rs','rs.article_id = {{%blog_article}}.id')->distinct();
            $query->innerJoin('{{%blog_tag}} as s','s.id = rs.tag_id')->distinct();
            $query->andFilterWhere(['s.id' => $this->tag_id]);
        }

        if (!empty($this->start_time)){
            $query->andWhere('created_at>="'.$this->start_time.'"');
        }

        if (!empty($this->end_time)){
            $query->andWhere('created_at<="'.$this->end_time.'"');
        }

        $query->orderBy(['created_at' => SORT_DESC]);
        return $dataProvider;
    }
}
