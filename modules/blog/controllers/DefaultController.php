<?php

namespace app\modules\blog\controllers;

use app\modules\blog\models\Article;
use app\modules\blog\models\Post;
use app\modules\blog\models\PostTag;
use app\modules\blog\models\Category;
use app\modules\blog\models\Tag;
use Yii;
use app\modules\blog\models\PostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;

/**
 * Default controller for the `blog` module
 */
class DefaultController extends Controller
{
    public $layout = '//column2';

    public function beforeAction($action)
    {
        $this->layout = $this->module->layout_frontend;
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        //var_dump($params);exit();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            //'title' => Yii::t('app', 'Articles'),
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionCategory($slug)
    {
        $categoryModel = Category::findOne(['slug' => $slug]);
        if ($categoryModel === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        $searchModel = new ArticleSearch();
        $params = Yii::$app->request->queryParams;
        $params['ArticleSearch']['category_id'] = $categoryModel->id;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $categoryModel->title,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionTag($slug)
    {
       $tagModel = PostTag::findOne(['slug' => $slug]);
        if ($tagModel === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
       
        $searchModel = new PostSearch();
        $params = Yii::$app->request->queryParams;
        //echo('Params');

        
        $params['PostSearch']['tag_id'] = $tagModel->id;
        //var_dump($params); 
        //exit();
        //var_dump($params);exit();
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            //'title' => 'Статьи с тегом ' . $tagModel->title,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionArchive($year, $month)
    {
        $searchModel = new ArticleSearch();
        $params = Yii::$app->request->queryParams;
        $params['ArticleSearch']['start_time'] = date('Y-m-d H:i:s', strtotime($year.'-'.$month.'-01 00:00:00'));
        $params['ArticleSearch']['end_time'] = date('Y-m-t 23:59:59', strtotime($year.'-'.$month.'-01'));
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Архив за ' . Yii::$app->formatter->asDate($year.'-'.$month.'-01', 'YYYY, LLLL'),
        ]);
    }

    /**
     * Displays a single Article model.
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        
        $model=Post::findOne(['id'=>$id]);
        //$query = Post::find()->where('tagNames = :tags',[':tags'=>$model->tagNames])->all();
        //var_dump($query->prepare(\Yii::$app->db->queryBuilder)->createCommand()->rawSql); exit();
        /*$dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);*/
        $timeNow = date('Y-m-d 19:00:00');
        //var_dump($timeNow);
        $timeThen = date('Y-m-01 00:00:00');
        $query = Post::find()
        ->where(['between', 'created_at',$timeThen, $timeNow]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
       // var_dump($dataProvider);exit();
       /* if(count($models) !==0){
            Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
        }*/
       
    
        //var_dump($query->prepare(\Yii::$app->db->queryBuilder)->createCommand()->rawSql);exit();
        

        return $this->render('view', [
            'model' => $model,
            'dataProvider'=>$dataProvider,
        ]);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = Article::findOne(['slug' => $slug])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
