<?php

namespace app\modules\blog\controllers;

use Imagine\Image\Box;
use Yii;
use app\modules\blog\models\Article;
use app\modules\blog\models\ArticleSearch;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\UploadedFile;


/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{


    //public $layot = '//blog';
   

   

    public function behaviors()
    {
      
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => $this->module->adminRoles,
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'images-get' => [
                'class' => '\vova07\imperavi\actions\GetImagesAction',
                'url' => Url::to(['/'], true).'uploads/blog/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/blog', // Or absolute path to directory where files are stored.
                'options' => ['only' => ['*.jpg', '*.jpeg', '*.png', '*.gif', '*.ico']], // These options are by default.
            ],
            'image-
            upload' => [
                'class' => '\vova07\imperavi\actions\UploadFileAction',
                'url' => Url::to(['/'], true).'uploads/blog/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/blog', // Or absolute path to directory where files are stored.
            ],
            'files-get' => [
                'class' => 'vova07\imperavi\actions\GetFilesAction',
                'url' => Url::to(['/'], true).'uploads/blog/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/blog', // Or absolute path to directory where files are stored.
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Url::to(['/'], true).'uploads/blog/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/blog', // Or absolute path to directory where files are stored.
                'uploadOnlyImage' => false, // For any kind of files uploading.
            ],
            'file-delete' => [
                'class' => '\vova07\imperavi\actions\DeleteFileAction',
                'url' => Url::to(['/'], true).'uploads/blog/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/blog', // Or absolute path to directory where files are stored.
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
       
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $image = UploadedFile::getInstance($model, 'file');
            if (!is_null($image)) {
                $model->image = $image->name;
                $path_parts = pathinfo($model->image);
                $ext = $path_parts['extension'];
                $model->image = "article_{$model->id}.{$ext}";
                $path = Yii::$app->basePath . '/web/images/articles/full/' . $model->image;
                $image->saveAs($path);
                $imagine = Image::getImagine();
                $image = $imagine->open($path);
                $image->resize(new Box(400, 300))->save(Yii::$app->basePath . '/web/images/articles/thumbs/' . $model->image, ['quality' => 70]);
                $model->save(false);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
//        $model->image = $model->image ? Yii::getAlias('@webroot') . $model->getFullImage() : '';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $image = UploadedFile::getInstance($model, 'file');
            if (!is_null($image)) {
                $model->image = $image->name;
                $path_parts = pathinfo($model->image);
                $ext = $path_parts['extension'];
                $model->image = "article_{$model->id}.{$ext}";
                $path = Yii::$app->basePath . '/web/images/articles/full/' . $model->image;
                $image->saveAs($path);
                $imagine = Image::getImagine();
                $image = $imagine->open($path);
                $image->resize(new Box(400, 300))->save(Yii::$app->basePath . '/web/images/articles/thumbs/' . $model->image, ['quality' => 70]);
            }
            if ($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);
        unlink(Yii::$app->basePath . '/web/images/articles/full/' . $model->image);
        unlink(Yii::$app->basePath . '/web/images/articles/thumbs/' . $model->image);
        $model->image = '';
        $model->save();
        $success=true;
        return json_encode($success);
    }
    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
