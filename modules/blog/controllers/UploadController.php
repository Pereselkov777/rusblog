<?php

namespace app\modules\blog\controllers;

use app\modules\blog\models\Photo;
use Yii;

class UploadController extends \yii\web\Controller
{

    public function beforeAction($action)
    {
        if ($action->id === 'index'||$action->id === 'crop') {    
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    } 

    public function actionIndex()
    {
        if (Yii::$app->request->isAjax) {
            //$data = Yii::$app->request->post();
            /*echo'<pre>';
            var_dump($_FILES['file']);
            var_dump($_POST);
            exit();
            echo'</pre>';*/
            $session = Yii::$app->session;
            if(isset($_FILES['file'])) {
                //echo'<pre>';
                //var_dump($_FILES['file']);
                //var_dump($session->get('post_id'));
                //var_dump($_POST['upload_photo_id']);
                //echo'</pre>';
                $post_id = Yii::$app->session->get('post_id');
                $session->remove('post_id');
                //var_dump($post_id);
                //exit();
                $model = new Photo();
                $model->post_id = $post_id;
                $model->status = 1;
                $model->img_full = $model->img_prev = '0.jpg';
                $model->create_time = date('Y-m-d H:i:s');
                $model->update_time = date('Y-m-d H:i:s');
                
               /* if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $model->img_full)) {
                    unlink(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $model->img_full );
                }
                if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $model->img_prev)) {
                    unlink(Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $model->img_prev );
                }
                if ($is_private_photo) {
                    if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/blurs/' . $model->img_blur)) {
                        unlink(Yii::getAlias('@webroot') . '/uploads/photos/blurs/' . $model->img_blur );
                    }
                }*/

                $path_parts = pathinfo($_FILES['file']['name']);
                $ext = $path_parts['extension'];
                //var_dump($_POST['upload_user_id']);
                //exit();
                //var_dump($post_id);
                $name = $post_id . '_' . time() . '.' . $ext;
                $uploadDirFull = '/uploads/full/';
                move_uploaded_file($_FILES['file']['tmp_name'], Yii::getAlias('@webroot') . $uploadDirFull . $name);
                if ($ext == 'png') {
                    $image = imagecreatefrompng(Yii::getAlias('@webroot') . '/uploads/full/' . $name);
                } else {
                    //var_dump(Yii::getAlias('@webroot') . '/uploads/full/' . $name); exit();
                    $image = imagecreatefromjpeg(Yii::getAlias('@webroot') . '/uploads/full/' . $name);
                }

                //  $this->blur(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $name);

                $filename = Yii::getAlias('@webroot') . '/uploads/thumbs/' . $name;
                         

                $thumb_width = 399;
                $thumb_height = 350;
                //                  $thumb_height = 225;

                $width = imagesx($image);
                $height = imagesy($image);

                $original_aspect = $width / $height;
                $thumb_aspect = $thumb_width / $thumb_height;

                if ($original_aspect >= $thumb_aspect) {
                    // If image is wider than thumbnail (in aspect ratio sense)
                    $new_height = $thumb_height;
                    $new_width = $width / ($height / $thumb_height);
                } else {
                    // If the thumbnail is wider than the image
                    $new_width = $thumb_width;
                    $new_height = $height / ($width / $thumb_width);
                }

                $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

                // Resize and crop
                imagecopyresampled($thumb,
                    $image,
                    0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                    0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                    0, 0,
                    $new_width, $new_height,
                    $width, $height);
                if ($ext == 'png') {
                    imagepng($thumb, $filename);
                } else {
                    imagejpeg($thumb, $filename, 80);
                }

                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->img_full = $model->img_prev = $name;
                $model->title = $name;
                if (!$model->save()) {
                    echo('STOP');
                    var_dump($model->getErrors());
                };
                return [
                   'result' => 'success',
                    'id' => $model->id,
                    'file_full' => $uploadDirFull . $name,
                    'file_prev' => '/uploads/thumbs/' . $name,
                    'post_id'=>$post_id,
                ];
               /* $fileName = $_FILES['file']['name'];
             
                echo '����: ' . $fileName . '<br>';
             
                //�������� ����� �� ������
                $uploadDir = Yii::getAlias('@webroot') . '/uploads/new/'; //���������� �� �������, ��� ����������� ������
             
                if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadDir . $fileName)) {
                        echo '�������� ������ ������� <br><pre>';
                        
                        print_r($_FILES); 
                        print_r($_POST); 
                        echo '</pre>';
                        die; 
                } else {
                    echo '�������� ����� �� �������!<br>';
                    var_dump($_FILES);
                }*/
            }
        }
    }

    public function actionCrop()
    {
        if (Yii::$app->request->isAjax && isset($_POST['upload_photo_id'])
        && isset($_POST['upload_post_id'])) {
            $photo_id = $_POST['upload_photo_id'];
            $post_id = $_POST['upload_post_id'];
            if ($photo_id) {
               $model = Photo::findOne(['id' => $photo_id]);
            } else {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'result' => 'fail',
                    'message' => 'Photo not found',
                ];
            }
            if (file_exists(Yii::getAlias('@webroot') . '/uploads/thumbs/' . $model->img_prev)) {
                unlink(Yii::getAlias('@webroot') . '/uploads/thumbs/' . $model->img_prev );
            }
            if (mime_content_type($_FILES['file']['tmp_name']) == 'image/png') {
                $ext = 'png';
            } else {
              $ext = 'jpg';
            }
            //$path_parts = pathinfo($_FILES['file']['name']);
            //$ext = $path_parts['extension'];
            $name = $_POST['upload_post_id'] . '_' . time() . '.' . $ext;
            $model->img_prev = $name;
            $uploadDirFull = '/uploads/thumbs/';
            $file = Yii::getAlias('@webroot') . $uploadDirFull . $model->img_prev;
            move_uploaded_file($_FILES['file']['tmp_name'], $file);

            //�������� �� 300�400
            if (mime_content_type($file) == 'image/png') {
                $image = imagecreatefrompng($file);
            } else {
                $image = imagecreatefromjpeg($file);
            }
            $thumb_width = 399;
            $thumb_height = 350;
//        $thumb_height = 225;

            $width = imagesx($image);
            $height = imagesy($image);

            $original_aspect = $width / $height;
            $thumb_aspect = $thumb_width / $thumb_height;

            if ($original_aspect >= $thumb_aspect) {
                // If image is wider than thumbnail (in aspect ratio sense)
                $new_height = $thumb_height;
                $new_width = $width / ($height / $thumb_height);
            } else {
                // If the thumbnail is wider than the image
                $new_width = $thumb_width;
                $new_height = $height / ($width / $thumb_width);
            }

            $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

// Resize and crop
            imagecopyresampled($thumb,
                $image,
                0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                0, 0,
                $new_width, $new_height,
                $width, $height);
            if (mime_content_type($file) == 'image/png') {
                imagepng($thumb, $file);
            } else {
                imagejpeg($thumb, $file, 80);
            }
            //����� �������

            if (!$model->save()) {
                var_dump($model->getErrors()); exit;
            }
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'result' => 'success',
                'id' => $model->id,
                'file' => $uploadDirFull . $model->img_prev,
            ];
        }
    }

}