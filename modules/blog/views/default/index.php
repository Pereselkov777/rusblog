<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\blog\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = $title;
//$this->params['breadcrumbs'][] = $this->title;

//exit();

?>

 <section class="home-slider owl-carousel ftco-degree-bg">
      <div class="slider-item bread-wrap" style="background-image: url('/r/images/bg_1.jpg');" data-stellar-background-ratio="0.5">
        <div id="scrolling" class="overlay"></div>
        <div class="container">
          <div class="row slider-text justify-content-center align-items-center">
            <div class="col-md-10 col-sm-12 ftco-animate mb-4 text-center">
              <p class="breadcrumbs"><span class="mr-2"><a href="/">Home</a></span> <span>Blog</span></p>
              <h1 class="mb-3 bread">Read our blog</h1>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="container">
      <div class="row slider-text justify-content-center align-items-center">
        <div class="col-md-10 col-sm-12 ftco-animate mb-4 text-center">
        
        <?php if (!empty($title)) { ?>
          <h1 class="mb-3 bread"> <h1><?=$title?></h1></h1>
      
        <?php } ?>
          
        </div>
      </div>
    </div>
    <section class="ftco-section ftco-degree-bg">
      <div class="container">
          <?php echo ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_singl',
                'layout' => "{items}",
                'itemOptions' => ['class' => 'col-md-4 ftco-animate'],
                'options' => ['class' => 'row'],
                'pager' => [
                    'firstPageLabel' => 'first',
                    'lastPageLabel' => 'last',
                    'nextPageLabel' => 'next',
                    'prevPageLabel' => 'previous',
                    'maxButtonCount' => 3,
                ],
                
          ]);?>
        <?= \app\modules\blog\widgets\PostTagWidget::widget([]) ?>  
        <div class="row mt-5">
          <div class="col text-center ml-auto">
            <div class="block-27">
                <?php
                        echo LinkPager::widget([
                            'pagination' => $dataProvider->getPagination(),
                            'options' => [
                              'class' => 'ml-auto',
                          ],
                        ]);
                    ?>
            </div>
          </div>
        </div>
      </div>
    </section>