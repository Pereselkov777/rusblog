<?php
use \yii\bootstrap\Html;
use yii\helpers\Url;


$photo = $model->getPhoto($model->id);

//exit();
//var_dump($photo['img_prev']); exit();

?>


    <div class="blog-entry">
        <a href="<?php echo Url::to(['/blog/default/view/?id=' . $model->id .''], true);?>" class="block-20"
         style="background-image: url(/uploads/thumbs/<?php echo  $photo['img_prev']?>);">
        </a>
        <div class="text p-4 d-block">
        <div class="meta mb-3">
            <div><a href="#"><?= $model->created_at ?></a></div>
            <div><a href="#"><?= $model->user_id ?></a></div>
            <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
        </div>
        <h3 class="heading"><?= Html::a($model->title, ['/blog/default/view', 'id' => $model->id]) ?></h3>
        </div>
    </div>
