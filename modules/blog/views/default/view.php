<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\disqus\Comments;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ListView;
/* @var $this yii\web\View */
/* @var $model app\modules\blog\models\Article */
//var_dump($model);exit;
//$this->title = $model->meta_title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'POsts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;

$photo = $model->getPhoto($model->id);
//var_dump($model); exit();
?>
<section class="home-slider owl-carousel ftco-degree-bg">
    <div class="slider-item bread-wrap" style="background-image: url('/r/images/bg_1.jpg');"
        data-stellar-background-ratio="0.5">
        <div id="scrolling" class="overlay"></div>
        <div class="container">
            <div class="row slider-text justify-content-center align-items-center">
                <div class="col-md-10 col-sm-12 ftco-animate mb-4 text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="/">Home</a></span> <span class="mr-2"><a
                                href="<?php echo Url::to('/blog/default/index',true);?>">Blog</a></span> <span>Single
                            Blog</span></p>
                    <h1 class="mb-3">Single Blog</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section ftco-degree-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ftco-animate">
                <h2 class="mb-3"><?= $model->title ?></h2>
                <img src="<?php echo Url::to(['/uploads/full/' . $photo['img_full'].'']);?>" alt="" class="img-fluid">

                <p style="color:black;"><?= $model->text ?></p>
                <!--       <div class="tag-widget post-tag-container mb-5 mt-5">
                    <div class="tagcloud">
                        <a href="#" class="tag-cloud-link">Life</a>
                        <a href="#" class="tag-cloud-link">Sport</a>
                        <a href="#" class="tag-cloud-link">Tech</a>
                        <a href="#" class="tag-cloud-link">Travel</a>
                    </div>
                </div>   !-->

                <!--    <div class="about-author d-flex p-5 bg-light">
              <div class="bio align-self-md-center mr-5">
                <img src="/r/images/person_1.jpg" alt="Image placeholder" class="img-fluid mb-4">
              </div>
              <div class="desc align-self-md-center">
                <h3>Lance Smith</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
              </div>
            </div>   !-->
                <?= Comments::widget([
              'shortname' => 'blog-site-14',
              //'identifier'=>$model->identifier,
            ]); ?>

            </div> <!-- .col-md-8 -->
            <div class="col-md-4 sidebar ftco-animate">
                <!-- <div class="sidebar-box">
              <form action="#" class="search-form">
                <div class="form-group">
                  <span class="icon fa fa-search"></span>
                  <input type="text" class="form-control" placeholder="Type a keyword and hit enter">
                </div>
              </form>
            </div>!-->
                <div class="sidebar-box ftco-animate">
                    <h3>Статьи в этом месяце</h3>
                    <?php echo ListView::widget([
                  'dataProvider' => $dataProvider,
                  'itemView' => '_recent',
                  'layout' => "{items}",
                  'itemOptions' => ['class' => 'block-21 mb-4 d-flex'],
                  'options' => ['class' => 'row'],
                  'pager' => [
                      'firstPageLabel' => 'first',
                      'lastPageLabel' => 'last',
                      'nextPageLabel' => 'next',
                      'prevPageLabel' => 'previous',
                      'maxButtonCount' => 3,
                  ],
                  
                ]);?>


                </div>

                <!-- <div class="sidebar-box ftco-animate">
              <h3>Paragraph</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
            </div>
          </div>!-->

            </div>
        </div>
</section> <!-- .section -->



<div class="pluso" data-background="#ebebeb" data-options="medium,square,line,horizontal,counter,theme=04"
    data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print"></div>
<?php /*
    <div class="block_comments">
        <h3>Комментарии</h3>
        <div><?php
            echo \dosamigos\disqus\CommentsCount::widget([
                'shortname' => Yii::$app->params['disqus_name'],
                'identifier' => \yii\helpers\Url::to(['view', 'slug' => $model->slug])
            ]); ?> </div>
<div><?php
            echo \dosamigos\disqus\Comments::widget([
                // see http://help.disqus.com/customer/portal/articles/472098-javascript-configuration-variables
                'shortname' => Yii::$app->params['disqus_name'],
                'identifier' => \yii\helpers\Url::to(['view', 'slug' => $model->slug])
            ]);?>
</div>
</div>
<?php */ ?>
</div>