<?php
use \yii\bootstrap\Html;
/* @var $model app\modules\blog\models\Article */
?>
<div class="single-blog pb-30">
    <div class="blog-pic single-img">
        <img src="<?= $model->getThumbImage() ?>" alt="<?= $model->title ?>">
        <div class="gallery-icon">
            <a class="image-popup" href="<?= $model->getFullImage() ?>">
                <i class="zmdi zmdi-zoom-in"></i>
            </a>
        </div>
    </div>
    <div class="blog-content">
        <h3><?= Html::a($model->title, ['/blog/default/view', 'slug' => $model->slug]) ?></h3>
        <h6><?= Yii::$app->formatter->asDatetime(strtotime($model->created_at), 'dd MMMM yyyy, hh:mm') ?></h6>
        <p><?= nl2br($model->annotation) ?></p>
        <?= Html::a('Читать дальше', ['/blog/default/view', 'slug' => $model->slug], ['class' => 'read']) ?>
    </div>
</div>

<!--
<div class="row">
    <div class="<?= empty($model->image) ? 'col-xs-12 col-md-12' : 'col-xs-12 col-md-9 col-lg-7' ?>">
        <h2><?= Html::a($model->title, ['/blog/default/view', 'slug' => $model->slug]) ?></h2>
        <p>
            <span class="glyphicon glyphicon-calendar"></span> <?= Yii::$app->formatter->asDatetime(strtotime($model->created_at), 'dd MMMM yyyy, hh:mm') ?>
            <?php /*, написал <span class="glyphicon glyphicon-user"></span> <a href="#"><?= $model->author->username ?></a> <?php */ ?>
        </p>

        <p><?= nl2br($model->annotation) ?></p>

        <?php if (!empty($model->tags)) {
            foreach ($model->tags as $tag) { ?>
                <?= Html::a('<span class="label label-gold">'.$tag->title.'</span>', ['tag', 'slug' =>$tag->slug ]) ?>
            <?php }
        } ?>

        <?php if (!empty($model->category)) { ?>
            <?= Html::a('<span class="label label-success">'.$model->category->title.'</span>', ['category', 'slug' =>$model->category->slug ]) ?>
        <?php } ?>

        <p>
            <br>
            <?= Html::a('Читать дальше', ['/blog/default/view', 'slug' => $model->slug], ['class' => 'btn btn-gold']) ?>
        </p>
    </div>
    <?php if (!empty($model->image)) { ?>
        <div class="col-xs-12 col-md-3 col-lg-5">
            <?= Html::a('<img src="'. $model->getThumbImage() .'" />', ['/blog/default/view', 'slug' => $model->slug], ['class' => 'thumbnail']) ?>
        </div>
    <?php } ?>
</div>

<hr/>
-->

