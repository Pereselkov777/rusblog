<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\blog\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'author.username',
            'category.title',
            'is_commentable',
            'is_enabled',
            'title',
            'slug',
            'meta_title',
            'keywords',
            'description',
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <h1><?= $model->title ?></h1>
    <?php if ($model->image) { ?>
        <p><img src="<?= $model->getFullImage() ?>" style="width: 100%"></p>
    <?php } ?>
    <p><?= nl2br($model->annotation)  ?></p>
    <p><?= $model->text  ?></p>

</div>
