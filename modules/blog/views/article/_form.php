<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\selectize\SelectizeTextInput;
use yii\helpers\Url;
use vova07\imperavi\Widget;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\modules\blog\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'category_id')->dropDownList(\app\modules\blog\models\Category::getAllCategoryList(),[
        'prompt' => 'Выберите категорию'
    ]) ?>

    <?= $form->field($model, 'file')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
            'multiple' => false,
        ],
        'pluginOptions' => [
            'showPreview' => true,
            'showRemove' => true,
            'showUpload' => true,
            'deleteUrl' => Url::toRoute(['article/delete-image', 'id' => $model->id]),
            'initialPreview' => $model->image ? $model->getFullImage() : false,
            'initialPreviewAsData'=>true,
            'overwriteInitial'=> false,
        ]
    ]); ?>

    <?= $form->field($model, 'is_commentable')->dropDownList(['Да', 'Нет']) ?>

    <?= $form->field($model, 'is_enabled')->dropDownList([1=>'Да', 0=>'Нет']) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tagNames')->widget(SelectizeTextInput::className(), [
        // calls an action that returns a JSON object with matched
        // tags
        'loadUrl' => ['tag/list'],
        'options' => ['class' => 'form-control'],
        'clientOptions' => [
            'plugins' => ['remove_button'],
            'valueField' => 'title',
            'labelField' => 'title',
            'searchField' => ['title'],
            'create' => true,
        ],
    ])->hint('Use commas to separate tags') ?>

    <?= $form->field($model, 'annotation')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'text')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'imageUpload' => Url::to(['article/image-upload']),
            'imageManagerJson' => Url::to(['article/images-get']),
            'fileUpload' => Url::to(['article/file-upload']),
            'fileManagerJson' => Url::to(['article/files-get']),
            'plugins' => [
                'imagemanager',
                'filemanager',
                'clips',
                'fullscreen',
            ],
            'clips' => [
                ['Lorem ipsum...', 'Lorem...'],
                ['red', '<span class="label-red">red</span>'],
                ['green', '<span class="label-green">green</span>'],
                ['blue', '<span class="label-blue">blue</span>'],
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
