<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use app\assets\App2Asset;


App2Asset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\modules\blog\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Articles');
$this->params['breadcrumbs'][] = $this->title;


?>

    <?php GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'author_id',
                'filter' => \app\models\User::getFullList(),
                'value' => function($data) {
                    return !empty($data->author) ? $data->author->username : '';
                }
            ],
            [
                'attribute' => 'category_id',
                'filter' => \app\modules\blog\models\Category::getAllCategoryList(),
                'value' => function($data) {
                    return !empty($data->category) ? $data->category->title : '';
                }
            ],
            'title',
            'slug',
            'annotation:ntext',
            'created_at',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);?>


