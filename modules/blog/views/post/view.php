<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\blog\models\Post */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'text',
            'user_id',
            'created_at',
            'tagNames',
            'slug',
        ],
    ]) ?>

</div>
<div class="row row-12 row-x-12 d-md-flex flex-md-equal w-100" data-lightgallery="group" id="public_photo_list">
        <div class="col-xs-12 col-md-4 bg-light text-center overflow-hidden js-upload-item" id="new_public_photo">
            <div class="bg-dark box-shadow mx-auto" style="width: 100%; height: 400px; border-radius: 21px;">
                <div class="p-3">
                    <h2 class="display-5 text-light">Add new public photo</h2>
                </div>
            </div>
            <div class="my-3">
                <p class="lead">
                    <button class="btn btn-primary js-show-upload" data-id="0" style="min-width: 240px;">Add</button>
                </p>
            </div>
        </div>
       
        

       
        <div class="col-xs-12 col-md-4 bg-light text-center overflow-hidden js-upload-item">
            <a class="thumbnail-light bg-dark box-shadow mx-auto" href="" data-lightgallery="item" style="width: 100%; height: 400px; border-radius: 21px;">
                <img class="thumbnail-light-image" src="" alt="" width="300" height="400">
            </a>
            <div class="my-3">
                <p class="lead">
                    <button id="update" class="btn btn-primary js-show-upload" data-id="" style="min-width: 240px;">Change</button>
                    <button class="btn btn-success js-avatar-upload mt-3 mb-3" data-id="" style="min-width: 240px;">Assign Avatar</button>
                    <button class="btn btn-danger js-photo-delete" data-id="">Delete</button>
                </p>
            </div>
        </div>
      
    </div>

</div>
<div class="modal" tabindex="-1" id="modal_photo_upload">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Change photo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <img id="crop_image" src="">
                </div>
                <div class="modal-footer" style="display: block">
                    <div>
                    <input type="hidden" id="upload_user_id" value="<?= Yii::$app->user->id ?>">
                    <input type="hidden" id="upload_photo_id" value="<?= $model->id?>">
                    <input type="file"  id="btn_upload" accept="image/*" /> or
                    <input type="text" id="url_photo_upload" value="" placeholder="URL" />
                    </div>
                    <div>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn_crop">Crop && Done</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
