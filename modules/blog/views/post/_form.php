<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\selectize\SelectizeTextInput;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model app\modules\blog\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'text')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            //'imageUpload' => Url::to(['article/image-upload']),
            //'imageManagerJson' => Url::to(['article/images-get']),
            //'fileUpload' => Url::to(['article/file-upload']),
            //'fileManagerJson' => Url::to(['article/files-get']),
            'plugins' => [
                //'imagemanager',
                //'filemanager',
                'clips',
                'fullscreen',
            ],
            'clips' => [
                ['Lorem ipsum...', 'Lorem...'],
                ['red', '<span class="label-red">red</span>'],
                ['green', '<span class="label-green">green</span>'],
                ['blue', '<span class="label-blue">blue</span>'],
            ],
        ],
    ]); ?>


    <?= $form->field($model, 'title')->textInput() ?>

    <?=  $form->field($model, 'tagNames')->widget(SelectizeTextInput::className(), [
                    'loadUrl' => ['/blog/post-tag/list'],
                    'options' => ['class' => 'form-control'],
                    'clientOptions' => [
                        'plugins' => ['remove_button'],
                        'valueField' => 'title',
                        'labelField' => 'title',
                        'searchField' => ['title'],
                        'create' => true,
                    ],
                ])->hint('Use commas to separate tags')?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>