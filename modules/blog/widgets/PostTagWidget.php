<?php
namespace app\modules\blog\widgets;

use app\modules\blog\models\Category;
use app\modules\blog\models\PostTag;
use Yii;
use yii\base\Widget;
use yii\bootstrap\Html;


class PostTagWidget extends Widget{
    public $maxTags=20;

    public function init(){
        parent::init();
    }

    public function run(){
        $tags = PostTag::findTagWeights($this->maxTags);
        //var_dump($tags); exit();
        return $this->render('postTagWidget', ['tags' => $tags]);
    }
}
?>