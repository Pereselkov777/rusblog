<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use budyaga\users\components\AuthChoice;
use wbraganca\fancytree\FancytreeWidget;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $tags array */

?>



<div class="panel panel-info portlet">
    <div class="panel-heading portlet-decoration">
        <div class="portlet-title">Облако тегов</div>
        
    </div>
    <div class="body portlet-content">
        <div id="tags_cloud">
          
            <div class="tag-widget post-tag-container mb-5 mt-5">
                <div class="tagcloud">
                    <?php 
                        foreach($tags as $slug=>$tag)
                        {
                            $link=Html::a($tag['title'], [Url::to(['/blog/default/tag']), 'slug'=>$slug]);
                            echo Html::tag('span', $link, [
                                    'class'=>'tag-cloud-link',
                                    'style'=>"color:white !important;",
                                ])."\n";
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
