<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use budyaga\users\components\AuthChoice;
use wbraganca\fancytree\FancytreeWidget;
use yii\web\JsExpression;

/* @var $this yii\web\View */

?>

<div class="panel panel-info portlet">
    <div class="panel-heading portlet-decoration">
        <div class="portlet-title">Архив статей</div>
    </div>
    <div class="body portlet-content">
        <div id="archive_blog">
            <ul class="nav nav-pills nav-stacked">
                <?php
                foreach ($models as $m)
                {
                    echo '<li>'.Html::a(Yii::$app->formatter->asDate(strtotime($m['created_at']), 'LLLL YYYY'),["archive","year"=>$m["year"],"month"=>$m["month"]]).'</li>';
                }
                ?>
            </ul>
        </div>
    </div>
</div>
