<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use budyaga\users\components\AuthChoice;
use wbraganca\fancytree\FancytreeWidget;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $tags array */

?>

<div class="panel panel-info portlet">
    <div class="panel-heading portlet-decoration">
        <div class="portlet-title">Облако тегов</div>
    </div>
    <div class="body portlet-content">
        <div id="tags_cloud">
           <?php
           foreach($tags as $slug=>$tag)
           {
               $link=Html::a($tag['title'], ['tag', 'slug'=>$slug]);
               echo Html::tag('span', $link, [
                       'class'=>'tag',
                       'style'=>"font-size:{$tag['weight']}pt",
                   ])."\n";
           }

           ?>
        </div>
    </div>
</div>
