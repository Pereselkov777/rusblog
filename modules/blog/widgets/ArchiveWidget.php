<?php
namespace app\modules\blog\widgets;

use app\modules\blog\models\Article;
use app\modules\blog\models\Category;
use app\modules\blog\models\Tag;
use Yii;
use yii\base\Widget;
use yii\bootstrap\Html;


class ArchiveWidget extends Widget{

    public function init(){
        parent::init();
    }

    public function run(){
        $models = Article::getArchived();
        return $this->render('archiveWidget', ['models' => $models]);
    }
}
?>