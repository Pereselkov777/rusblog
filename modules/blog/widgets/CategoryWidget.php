<?php
namespace app\modules\blog\widgets;

use app\modules\blog\models\Category;
use Yii;
use yii\base\Widget;


class CategoryWidget extends Widget{
    public $type = 1; //1 - vacancy, 2 - resume

    public function init(){
        parent::init();
    /*    if($this->type===1){
            $this->type= 'Welcome User';
        }else{
            $this->type= 'Welcome '.$this->message;
        }*/
    }


    public function run(){
        $result_menu='<ul class="list-group">';
        $res=Category::find()->where('parent_id is null ')->all();
//        var_dump($res); exit;
        $level = 0;
        foreach( $res as $row)
        {
            Category::rubrics_menu($row, $level, $result_menu);
        }
        $result_menu.='</ul>';
//        $models = CategoryService::find()->all();
 //       $models = CategoryService::getHierarchy(true);
   //     $menu_arr = \app\models\Service::run_category();
        return $this->render('categoryWidget', [
//            'models' =>$models,
            'rubrics' => $result_menu
        ]);
    }
}
?>