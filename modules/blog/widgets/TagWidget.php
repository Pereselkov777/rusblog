<?php
namespace app\modules\blog\widgets;

use app\modules\blog\models\Category;
use app\modules\blog\models\Tag;
use Yii;
use yii\base\Widget;
use yii\bootstrap\Html;


class TagWidget extends Widget{
    public $maxTags=20;

    public function init(){
        parent::init();
    }

    public function run(){
        $tags = Tag::findTagWeights($this->maxTags);
        return $this->render('tagWidget', ['tags' => $tags]);
    }
}
?>