<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_article`.
 */
class m180307_062116_create_blog_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%blog_article}}', [
            'id' => $this->primaryKey()->unsigned()->comment('ID'),
            'author_id' => $this->integer()->comment('Автор'),
            'category_id' => $this->integer()->unsigned()->comment('Категория'),
            'image' => $this->string(255)->comment('Изображение'),
            'slug' => $this->string(255)->unique()->notNull()->comment('Алиас'),
            'title' => $this->string(255)->notNull()->comment('Заголовок'),
            'is_commentable' => $this->tinyInteger(1)->notNull()->comment('Разрешено комментировать'),
            'is_enabled' => $this->tinyInteger(1)->notNull()->comment('Включено'),
            'annotation' => $this->text()->notNull()->comment('Аннотация'),
            'text' => $this->getDb()->getSchema()->createColumnSchemaBuilder('LONGTEXT')->notNull()->comment('Текст'),
            'meta_title' => $this->string(255)->comment('Метатеги. Заголовок'),
            'keywords' => $this->string(255)->comment('Метатеги. Ключевые слова'),
            'description' => $this->string(255)->comment('Метатеги. Описание'),
            'created_at' => $this->dateTime()->comment('Создано'),
            'updated_at' => $this->dateTime()->comment('Обновлено'),
        ]);
        $this->createIndex('idx_blog_article_category_id', '{{%blog_article}}', 'category_id');
        $this->createIndex('idx_blog_article_author_id', '{{%blog_article}}', 'author_id');
        $this->createIndex('idx_blog_article_created_at', '{{%blog_article}}', 'created_at');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%blog_article}}');
    }
}
