<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_tag`.
 */
class m180307_061852_create_blog_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%blog_tag}}', [
            'id' => $this->primaryKey()->unsigned()->comment('ID'),
            'slug' => $this->string(32)->unique()->notNull()->comment('Алиас'),
            'title' => $this->string(32)->unique()->notNull()->comment('Заголовок'),
            'created_at' => $this->dateTime()->comment('Создано'),
            'weight' => $this->integer()->comment('Вес'),
        ]);
        $this->createIndex('idx_blog_tag_weight', '{{%blog_tag}}', 'weight');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%blog_tag}}');
    }
}
