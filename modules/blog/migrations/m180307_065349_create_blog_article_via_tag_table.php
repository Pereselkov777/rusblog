<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_article_via_tag`.
 */
class m180307_065349_create_blog_article_via_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%blog_article_via_tag}}', [
            'article_id' => $this->integer()->unsigned()->notNull(),
            'tag_id' => $this->integer()->unsigned()->notNull(),
        ]);
        $this->addPrimaryKey(null, '{{%blog_article_via_tag}}', ['article_id','tag_id']);
        $this->createIndex('idx_article_id', '{{%blog_article_via_tag}}', 'article_id');
        $this->createIndex('idx_tag_id', '{{%blog_article_via_tag}}', 'tag_id');

        $this->addForeignKey('fg_blog_article_via_tag_article_id', '{{%blog_article_via_tag}}', 'article_id', '{{%blog_article}}', 'id', 'CASCADE', null );
        $this->addForeignKey('fg_blog_article_via_tag_tag_id','{{%blog_article_via_tag}}', 'tag_id', '{{%blog_tag}}', 'id', 'CASCADE', null );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%blog_article_via_tag}}');
    }
}
