<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';



$config = [
    'id' => getenv('APP_ID'),
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [
        'comment' => [
            'class' => 'yii2mod\comments\Module' ,
            'controllerMap' => [
                'manage' => [
                    'class' => 'yii2mod\comments\controllers\ManageController',
                    'accessControlConfig' => [
                        'class' => 'yii\filters\AccessControl',
                        'rules' => [
                            [
                                'allow' => true,
                                'roles' => ['administrator'],
                            ],
                        ],
                    ],
                ],
             ],
       ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'modelMap' => [
                'User' => 'app\models\User',
                'Profile' => 'app\models\Profile',
            ],
            'adminPermission' => 'administrator',
         //'admins' => ['admin']
         
        ],
        'rbac' => 'dektrium\rbac\RbacWebModule',
        'pages' => [
            'class' => 'bupy7\pages\Module',
            'tableName' => '{{%page}}',
            'layout' => '//admin',
            'controllerMap' => [
                'manager' => [
                    'class' => 'bupy7\pages\controllers\ManagerController',
                    'as access' => [
                        'class' => yii\filters\AccessControl::className(),
                        'ruleConfig' => [
                            'class' => yii\filters\AccessRule::className(),
                        ],
                        'rules' => [
                            [
                                'allow' => true,
                                'roles' => ['administrator'],
                            ],
                        ],
                    ],
                ],
                'default' => [
//                    'class' => 'bupy7\pages\controllers\DefaultController',
                    'class' => 'app\controllers\PageController',
                    'on beforeAction' => function($event) {
                        // see content of $event
                        $event->action->controller->layout = '/main';
                    }
                ],
            ],
            'pathToImages' => '@webroot/images',
            'urlToImages' => '@web/images',
            'pathToFiles' => '@webroot/files',
            'urlToFiles' => '@web/files',
            'uploadImage' => true,
            'uploadFile' => true,
            'addImage' => true,
            'addFile' => true,
        ],
        'messenger' => [
            'class' => \nanson\messenger\Messenger::className(),
        ],
        'blog' => [
            'class' => 'app\modules\blog\Module',
            'layout' => '//blog',
        ],
        'api' => [
            'class' => 'app\modules\api\Module',
        ],
    ],
    'components' => [
        'assetManager' => [
            'appendTimestamp' => true,
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-purple',
                ],
               
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'xK59rkkEv6NOWWJRIJS5Hg96hZb_dcSU',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => getenv('REDIS_DB'),
        ],
        'authManager'=>[
            'class' => 'dektrium\rbac\components\DbManager',
            'defaultRoles' => ['user'],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
//            'defaultRoles' => ['admin', 'viewer'],
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity',
                'httpOnly' => false,
                'domain' => '.' . $params['domain'],
            ],
            'loginUrl' => ['/user/security/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'transport' => [
                    'class' => 'Swift_SmtpTransport',
                    'host' => 'smtp.yandex.ru',
                    'username' => getenv('APP_MAILER_USERNAME'),
                    'password' => getenv('APP_MAILER_PASSWORD'),
                    'port' => 465,
                    'encryption' => 'ssl',
             ],
            'enableSwiftMailerLogging' =>false,
            'useFileTransport' => false,
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app'       => 'app.php',
                        'app/error' => 'error.php',
                        'language'  => 'language.php',
                    ],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['api/default']
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['api/article']
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['api/category']
                ],
                [
                    'pattern' => '/blog',
                    'route' => 'blog/default/index',
                ],
                [
                    'pattern' => 'blog/<slug:[-a-zA-Z _0-9]+>',
                    'route' => 'blog/default/view',
                    'defaults' => ['slug' => null],
                    'suffix'=>'.html'
                ],
                [
                    'pattern' => '/blog/category/<slug:[-a-zA-Z _0-9]+>',
                    'route' => 'blog/default/category',
                    'defaults' => ['slug' => null],
                    'suffix'=>'/'
                ],
                [
                    'pattern' => '/blog/tag/<slug:[-a-zA-Z _0-9]+>',
                    'route' => 'blog/default/tag',
                    'defaults' => ['slug' => null],
                    'suffix'=>'/'
                ],
                [
                    'pattern' => '/blog/archive/<year:[0-9]+>/<month:[0-9]+>',
                    'route' => 'blog/default/archive',
                    'defaults' => ['year' => null,'month' => null],
                    'suffix'=>'/'
                ],
                'sitemap.xml'=>'sitemap/index',
                [
                    'pattern' => '/robots',
                    'route' => 'sitemap/robots',
                    'suffix'=>'.txt'
                ],
                '/agreement' => 'site/agreement',
                '/contacts' => 'site/contact',
                [
                    'pattern' => '/affiliate/view/<slug:[-a-zA-Z 0-9]+>',
                    'route' => 'affiliate/view',
                    'defaults' => ['slug' => null],
                    'suffix'=>'.html'
                ],

                'pages/manager' => 'pages/manager/index',
                'pages/<page:[\w-]+>' => 'pages/default/index',
            ],
        ],

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
