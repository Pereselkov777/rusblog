<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class App2Asset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,700,800",
        //"https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false",
        "r/css/open-iconic-bootstrap.min.css",
        "r/css/animate.css",
        "r/css/owl.carousel.min.css",
        "r/css/owl.theme.default.min.css",
        "r/css/magnific-popup.css",
        "r/css/aos.css",
        "r/css/ionicons.min.css",
        "r/css/bootstrap-datepicker.css",
        "r/css/jquery.timepicker.css",
        "r/css/flaticon.css",
        "r/css/icomoon.css",
        "r/css/style.css",
        "r/css/icons.css"
        //'css/site.css',
        //"css/blog/style.css",
    ];
    public $js = [
        "r/js/jquery.min.js",
        "r/js/jquery-migrate-3.0.1.min.js",
        "r/js/popper.min.js",
        "r/js/bootstrap.min.js",
        "r/js/jquery.easing.1.3.js",
        "r/js/jquery.waypoints.min.js",
        "r/js/jquery.stellar.min.js",
        "r/js/owl.carousel.min.js",
        "r/js/jquery.magnific-popup.min.js",
        "r/js/aos.js",
        "r/js/jquery.animateNumber.min.js",
        "r/js/bootstrap-datepicker.js",
        "r/js/jquery.timepicker.min.js",
        //"/assets/32499985/js/comment.js?v=1617368003",
        //"r/https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false",
        //"r/js/google-map.js",
        "r/js/main.js",
        "js/disqus.js",

        "r/js/jquery.stellar.min.js",
        //"r/js/main.js",
    ];
    public $depends = [
        'rmrevin\yii\fontawesome\cdn\AssetBundle',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}