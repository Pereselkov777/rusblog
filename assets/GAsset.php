<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use \yii\web\View;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class GAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        
    ];
    public $js = [
        "https://www.googletagmanager.com/gtag/js?id=G-DEP47RCZ6M",
        'js/gtag.js',
      
        
    ];
    public $jsOptions = [
        'position' => View::POS_HEAD,

        'async' => 'async'
    ];
}