<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Montserrat:400,700%7CMuli:400,700',
        'css/site.css',
        //'css/blog/bootstrap.min.css',
        //"css/blog/font-awesome.min.css",
        "css/blog/style.css",
        "css/photo_upload.css",
    ];
    public $js = [
        "js/jquery.min.js",
        "js/bootstrap.min.js",
        "js/jquery.stellar.min.js",
        "js/main.js",
        "js/upload_photo.js",
        
    ];
    public $depends = [
        'rmrevin\yii\fontawesome\cdn\AssetBundle',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
