<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BlogAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Playfair+Display%7COpen+Sans:400,600',
        '/css/fonts.css',
        '/css/magnific-popup.css',
        '/css/themify-icons.css',
        '/css/nice-select.css',
        "css/flaticon.css",
        "css/gijgo.css",
        "css/animate.css",
        "css/slicknav.css",
        "css/style.css",
    ];
    public $js = [
        "js/vendor/modernizr-3.5.0.min.js",
        "js/vendor/jquery-1.12.4.min.js",
        "js/popper.min.js",
        "js/owl.carousel.min.js",
        "js/isotope.pkgd.min.js",
        "js/ajax-form.js",
        "js/waypoints.min.js",
        "js/jquery.counterup.min.js",
        "js/imagesloaded.pkgd.min.js",
        "js/scrollIt.js",
        "js/jquery.scrollUp.min.js",
        "js/wow.min.js",
        "js/nice-select.min.js",
        "js/jquery.slicknav.min.js",
        "js/jquery.magnific-popup.min.js",
        "js/plugins.js",
        "js/gijgo.min.js",
        "js/contact.js",
        "js/jquery.ajaxchimp.min.js",
        "js/jquery.form.js",
        "js/jquery.validate.min.js",
        "js/mail-script.js",
        "js/main.js",
        "js/disqus.js"
       
    ];
    public $depends = [
        'rmrevin\yii\fontawesome\cdn\AssetBundle',
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
