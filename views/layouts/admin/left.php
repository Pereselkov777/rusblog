<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=  Yii::$app->user->identity->profile->photo ?>" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?=  Yii::$app->user->identity->profile->name ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Управление', 'options' => ['class' => 'header']],
                    ['label' => 'Панель управления', 'icon' => 'user', 'url' => ['/blog/default'], 'visible' => Yii::$app->user->can('administrator')],
                    ['label' => 'Пользователи', 'icon' => 'navicon', 'url' => ['/user/admin/index'], 'visible' => Yii::$app->user->can('administrator')],
                    ['label' => 'Чанки', 'icon' => 'navicon', 'url' => ['/chunk/index'], 'visible' => Yii::$app->user->can('administrator')],
                    ['label' => 'Категории', 'icon' => 'bars', 'url' => ['/blog/category'], 'visible' => Yii::$app->user->can('administrator')],
                    ['label' => 'Тэги', 'icon' => 'navicon', 'url' => ['/blog/tag/'], 'visible' => Yii::$app->user->can('administrator')],
                    ['label' => 'Статьи', 'icon' => 'navicon', 'url' => ['/blog/article'], 'visible' => Yii::$app->user->can('administrator')],
                    ['label' => 'Страницы', 'icon' => 'navicon', 'url' => ['/pages/manager'], 'visible' => Yii::$app->user->can('administrator')],
                    ['label' => 'События', 'icon' => 'navicon', 'url' => ['/event/admin'], 'visible' => Yii::$app->user->can('administrator')],
                    ['label' => 'Галерея', 'icon' => 'navicon', 'url' => ['/gallery/admin'], 'visible' => Yii::$app->user->can('administrator')],
                    ['label' => 'Регистрации с лендингов', 'icon' => 'navicon', 'url' => ['/landing-signup/index'], 'visible' => Yii::$app->user->can('administrator')],
                    ['label' => 'Сообщения с лендингов', 'icon' => 'navicon', 'url' => ['/landing-mail/index'], 'visible' => Yii::$app->user->can('administrator')],
                    ['label' => 'Подписчики с лендингов', 'icon' => 'navicon', 'url' => ['/landing-subscriber/index'], 'visible' => Yii::$app->user->can('administrator')],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    ['label' => 'Logout', 'icon' => 'file-code-o', 'url' => ['/site/logout'], 'template'=>'<a href="{url}" data-method="post">{label}</a>',  'visible' => !Yii::$app->user->isGuest],
                ],

/*                'items' => [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Some tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],*/
            ]
        ) ?>

    </section>

</aside>
