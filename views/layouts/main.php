<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\App2Asset;
use app\assets\AppAsset;
//use \yii\bootstrap\Html;
use yii\helpers\Url;
use app\assets\GAsset;

//$isRelative = Url::isRelative('http://dev.pythonclassic.com');var_dump($isRelative); exit();
App2Asset::register($this);
GAsset::register($this);
//AppAsset::register($this);

//GoogleAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>

    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        <div class="container pt-0">
            <a class="navbar-brand" href="/">Pythonclassic</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="oi oi-menu"></span> Menu
            </button>

            <div class="collapse navbar-collapse" id="ftco-nav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active"><a href="/" class="nav-link">Home</a></li>
                    <?php if (Yii::$app->user->can('administrator')): ?>

                    <li class="nav-item"><a href="/blog/post/index" class="nav-link">Admin Panel</a></li>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->isGuest): ?>
                    <li class="nav-item"><a href="/user/security/login" class="nav-link">Admins</a></li>
                    <?php endif; ?>
                    <?php if (!Yii::$app->user->isGuest): ?>
                    <li class="nav-item"><a href="/site/logout" class="nav-link">Logout</a></li>
                    <?php endif; ?>
                    <!--        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="room.html" id="dropdown04" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Portfolio</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown04">
                            <a class="dropdown-item" href="portfolio.html">Portfolio</a>
                            <a class="dropdown-item" href="portfolio-single.html">Portfolio Single</a>
                        </div>
                    </li>       !-->
                    <li class="nav-item"><a href="<?php echo Url::to('/blog/default/index',true);?>"
                            class="nav-link">Blog</a></li>
                    <!--  
                                 <li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li>    !-->

                    <li class="nav-item">
                        <?= Html::a('Go To Tests', 'http://dev.pythonclassic.com/login', ['class' => 'btn btn-primary btn-outline py-3']) ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="section">
        <?= $content ?>
    </div>
    <!-- button scroll to top -->
    <a href="#top" class="button button_top js-button-top"><i class="button__icon icon-caret-up absolute-centered"
            style="padding-left: 17px; padding-top: 16px;"></i></a>
    <!-- end button scroll to top -->
    <footer class="ftco-footer ftco-bg-dark ftco-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Pythonclassic</h2>
                        <p>A small but handy app for python developers for self-teaching and programming experiments.
                        </p>
                        <ul class="ftco-footer-social list-unstyled float-md-left float-lft">
                            <li class="ftco-animate">

                                <a href="https://www.youtube.com/channel/UCrrwDQIk4Qys3O9CvoGAYuQ"><span
                                        class="icon-youtube"></span></a>
                            </li>
                            <li class="ftco-animate"><a href="https://github.com/rperesy"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path
                                            d="M12 0c-6.626 0-12 5.373-12 12 0 5.302 3.438 9.8 8.207 11.387.599.111.793-.261.793-.577v-2.234c-3.338.726-4.033-1.416-4.033-1.416-.546-1.387-1.333-1.756-1.333-1.756-1.089-.745.083-.729.083-.729 1.205.084 1.839 1.237 1.839 1.237 1.07 1.834 2.807 1.304 3.492.997.107-.775.418-1.305.762-1.604-2.665-.305-5.467-1.334-5.467-5.931 0-1.311.469-2.381 1.236-3.221-.124-.303-.535-1.524.117-3.176 0 0 1.008-.322 3.301 1.23.957-.266 1.983-.399 3.003-.404 1.02.005 2.047.138 3.006.404 2.291-1.552 3.297-1.23 3.297-1.23.653 1.653.242 2.874.118 3.176.77.84 1.235 1.911 1.235 3.221 0 4.609-2.807 5.624-5.479 5.921.43.372.823 1.102.823 2.222v3.293c0 .319.192.694.801.576 4.765-1.589 8.199-6.086 8.199-11.386 0-6.627-5.373-12-12-12z" />
                                    </svg></span></a></li>
                            <!--<li class="ftco-animate"><a href="#​"><span class="icon-facebook"></span></a></li>-->
                        </ul>
                    </div>
                </div>
                <div class="col-md">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Quick Links</h2>
                        <ul class="list-unstyled">
                            <li><a href="#" class="py-2 d-block">Home</a></li>
                            <li><a href="/blog/default/index" class="py-2 d-block">Blog</a></li>
                            <!--<li><a href="#" class="py-2 d-block">Services</a></li>
                            <li><a href="#" class="py-2 d-block">Portfolio</a></li>
                            <li><a href="#" class="py-2 d-block">Contact</a></li>
                            <li><a href="#" class="py-2 d-block">Privacy</a></li>-->
                        </ul>
                    </div>
                </div>
                <div class="col-md">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Contact Information</h2>
                        <ul class="list-unstyled">
                            <!--<li><a href="#" class="py-2 d-block">198 West 21th Street, Suite 721 New York NY 10016</a>
                            </li>
                            <li><a href="#" class="py-2 d-block">+ 1235 2355 98</a></li>-->
                            <li><a href="#" class="py-2 d-block">mail@pythonclassic.com</a></li>
                            <li><a href="https://discord.com/invite/HXs2tm9" class="py-2 d-block">Discord server</a>
                            </li>
                            <!--<li><a href="#" class="py-2 d-block">email@email.com</a></li>-->
                        </ul>
                    </div>
                </div>
                <!--<div class="col-md">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Newsletter</h2>
                        <p>Far far away, behind the word mountains, far from the countries.</p>
                        <form action="#" class="subscribe-form">
                            <div class="form-group">
                                <span class="icon icon-paper-plane"></span>
                                <input type="text" class="form-control" placeholder="Subscribe">
                            </div>
                        </form>
                    </div>
                </div>-->
            </div>
            <div class="row">
                <div class="col-md-12 text-center">

                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy; All rights reserved
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </footer>



    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00" />
        </svg></div>



    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>