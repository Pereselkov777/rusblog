<?php
use yii\helpers\Html;
use app\components\AuthorizationWidget;
use app\components\LocationWidget;
use yii\widgets\Menu;
use yii\bootstrap\Alert;
?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<?= $this->render('//_alert', ['module' => Yii::$app->getModule('user')]) ?>

      <div class="row">
            <div class="col-md-3">
                  <?= $this->render('_cabinet_menu') ?>
            </div>
            <div class="col-md-9">
                  <?= $content ?>
            </div>

      </div>

<?php $this->endContent();