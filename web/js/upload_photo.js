$(document).ready(function () {
  var cropper;
  var uploadItem;
  var is_new_file = false;
  var isNoviBuilder = false;
  function initEvents() {
    $(".js-show-upload").click(function () {
      var photo_id = $(this).data("id");

      $("#upload_photo_id").val(photo_id);

      uploadItem = $(this).closest(".js-upload-item");
      if (photo_id) {
        is_new_file = false;
        var img_full = uploadItem.find(".thumbnail-light");
        var full_link = img_full.attr("href");
        $("#modal_photo_upload").modal("show");
        initCropper(full_link);
      } else {
        console.log("up2");
        is_new_file = true;
        if (cropper) {
          cropper.destroy();
        }
        $("#crop_image").attr("src", "");
        $("#modal_photo_upload").modal("show");
      }
    });
    $(".js-avatar-upload-manager").click(function managerUpload() {
      console.log("AJ");
      var photo_id = $(this).data("id");
      var user_id = $("#upload_user_id").val();
      console.log(photo_id, "ID");
      $("#upload_photo_id").val(photo_id);
      uploadItem = $(this).closest(".js-upload-item");
      console.log(uploadItem, "ITEM");
      console.log($("#remove").data("id"), "RE");
      var current_id = $("#add_av").data("id");
      console.log($("#add_av").data("id"), "ADD");

      if (photo_id != current_id) {
        var img_prev = uploadItem.find(".thumbnail-light-image");
        console.log(uploadItem, "FIND");
        var prev_link = img_prev.attr("src");
        console.log(prev_link, "LINK");
        $.ajax({
          type: "POST",
          url: "/upload-avatar/save",
          data: { link: prev_link, id: photo_id, user_id: user_id },
          success: function (response) {
            console.log(response, "RES");
            var html =
              '<img class="card-img-top" src="' +
              prev_link +
              '" alt="" data-id="' +
              photo_id +
              '" >';
            console.log($("#remove"));
            $("#remove").empty().html(html);
          },
          error: function (exception) {
            console.log(exception.responseText);
          },
        });
      } else {
        alert("You are already using this photo");
      }
    });

    $(".js-avatar-upload").click(function Upload() {
      console.log("AJ");
      var photo_id = $(this).data("id");
      console.log(photo_id, "ID");
      $("#upload_photo_id").val(photo_id);
      uploadItem = $(this).closest(".js-upload-item");
      console.log(uploadItem, "ITEM");
      console.log($("#remove").data("id"), "RE");
      var current_id = $("#add_av").data("id");
      console.log($("#add_av").data("id"), "ADD");

      if (photo_id != current_id) {
        var img_prev = uploadItem.find(".thumbnail-light-image");
        console.log(uploadItem, "FIND");
        var prev_link = img_prev.attr("src");
        console.log(prev_link, "LINK");
        $.ajax({
          type: "POST",
          url: "/avatar/save",
          data: { link: prev_link, id: photo_id },
          success: function (response) {
            console.log(response, "RES");
            var html =
              '<img class="card-img-top" src="' +
              prev_link +
              '" alt="" data-id="' +
              photo_id +
              '" >';
            console.log($("#remove"));
            $("#remove").empty().html(html);
          },
          error: function (exception) {
            console.log(exception);
          },
        });
      } else {
        alert("You are already using this photo");
      }
    });

    $("#update").click(function () {
      console.log("up");
      Upload();
    });
    $("#updateManager").click(function () {
      console.log("UpdateManager");
    });

    $(".js-photo-delete").click(function () {
      if (confirm("Delete photo?")) {
        var form_data = new FormData();
        console.log($(this), "TH");
        console.log($(this).data("id"), "DEL");
        form_data.append("upload_photo_id", $(this).data("id"));
        form_data.append("upload_user_id", $("#upload_user_id").val());
        var elem = $(this);
        $.ajax({
          url: "/upload/delete?id=" + $(this).data("id"),
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,
          dataType: "JSON",
          type: "post",
          success: function (response) {
            elem.closest(".js-upload-item").remove();
            console.log(response, "RES");
          },
        });
      }
    });

    /* var plugins = {
      lightGallery: $("[data-lightgallery='group']"),
      lightGalleryItem: $("[data-lightgallery='item']"),
      lightDynamicGalleryItem: $("[data-lightgallery='dynamic']"),
      owl: $(".owl-carousel"),
    };

    // lightGallery
    /* if (plugins.lightGallery.length) {
      for (var i = 0; i < plugins.lightGallery.length; i++) {
        initLightGallery(plugins.lightGallery[i]);
      }
    }*/

    // lightGallery item
    /* if (plugins.lightGalleryItem.length) {
      // Filter carousel items
      var notCarouselItems = [];

      for (var z = 0; z < plugins.lightGalleryItem.length; z++) {
        if (
          !$(plugins.lightGalleryItem[z]).parents(".owl-carousel").length &&
          !$(plugins.lightGalleryItem[z]).parents(".swiper-slider").length &&
          !$(plugins.lightGalleryItem[z]).parents(".slick-slider").length
        ) {
          notCarouselItems.push(plugins.lightGalleryItem[z]);
        }
      }

      plugins.lightGalleryItem = notCarouselItems;

      for (var i = 0; i < plugins.lightGalleryItem.length; i++) {
        initLightGalleryItem(plugins.lightGalleryItem[i]);
      }
    }*/
  }
  /**
   * @desc Initialize the gallery with one image
   * @param {object} itemToInit - jQuery object
   * @param {string} addClass - additional gallery class
   */
  /* function initLightGalleryItem(itemToInit, addClass) {
    itemToInit = $(itemToInit);
    if (!isNoviBuilder) {
      if (
        itemToInit.length &&
        itemToInit.attr("href").split("http").length > 1
      ) {
        addClass = "lightgallery-iframe";
      }
      itemToInit.lightGallery({
        selector: "this",
        addClass: addClass,
        counter: false,
        iframeMaxWidth: "90%",
        youtubePlayerParams: {
          modestbranding: 1,
          showinfo: 1,
          rel: 1,
          controls: 1,
        },
        vimeoPlayerParams: {
          byline: 0,
          portrait: 0,
        },
      });
    }
  }*/

  /**
   * @desc Initialize the gallery with set of images
   * @param {object} itemsToInit - jQuery object
   * @param {string} addClass - additional gallery class
   */
  /* function initLightGallery(itemsToInit, addClass) {
    if (!isNoviBuilder) {
      $(itemsToInit).lightGallery({
        thumbnail: $(itemsToInit).attr("data-lg-thumbnail") !== "false",
        selector: "[data-lightgallery='item']",
        autoplay: $(itemsToInit).attr("data-lg-autoplay") === "true",
        pause: parseInt($(itemsToInit).attr("data-lg-autoplay-delay")) || 5000,
        addClass: addClass,
        mode: $(itemsToInit).attr("data-lg-animation") || "lg-slide",
        loop: $(itemsToInit).attr("data-lg-loop") !== "false",
      });
    }
  }*/

  function initCropper(full_link) {
    if (cropper) {
      cropper.destroy();
    }
    $("#crop_image").attr("src", full_link);
    var image = document.getElementById("crop_image");
    cropper = new Cropper(image, {
      aspectRatio: 3 / 4,
      minCropBoxWidth: 300,
      minCropBoxHeight: 400,
      crop(event) {
        console.log(event.detail.x);
        console.log(event.detail.y);
        console.log(event.detail.width);
        console.log(event.detail.height);
        console.log(event.detail.rotate);
        console.log(event.detail.scaleX);
        console.log(event.detail.scaleY);
      },
    });
  }

  initEvents();

  $("#btn_upload").change(function () {
    console.log("btn_upload");
    var file_data = $(this).prop("files")[0];
    var form_data = new FormData();

    //form_data.append("is_private_photo", $("#is_private_photo").val());
    form_data.append("upload_photo_id", $("#upload_photo_id").val());
    //form_data.append("upload_user_id", $("#upload_user_id").val());
    form_data.append("file", file_data, file_data["name"]);

    //console.log(form_data[0], "FORMDATA");
    $.ajax({
      url: "/blog/upload/index", // point to server-side PHP script
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      dataType: "JSON",
      type: "post",
      success: function (data) {
        console.log("data", data);
        //console.log("upload_photo_id", data.id);
        $("#upload_photo_id").val(data.id);
        $("#upload_post_id").val(data.post_id);
        console.log(data, "DATA");
        initCropper(data.file_full);
      },
    });
  });

  $("#url_photo_upload").on("keyup", function (e) {
    console.log("URL?");
    if (e.key === "Enter" || e.keyCode === 13) {
      var form_data = new FormData();
      console.log("ID load", $("#upload_photo_id").val());
      form_data.append("is_private_photo", $("#is_private_photo").val());
      form_data.append("upload_photo_id", $("#upload_photo_id").val());
      form_data.append("upload_user_id", $("#upload_user_id").val());

      form_data.append("url", $("#url_photo_upload").val());
      //alert(form_data);
      $.ajax({
        url: "/upload/url", // point to server-side PHP script
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        dataType: "JSON",
        type: "post",
        success: function (data) {
          console.log("data", data);
          console.log("upload_photo_id", data.id);
          $("#upload_photo_id").val(data.id);
          initCropper(data.file_full);
        },
      });
    }
  });
  $("#btn_crop").click(function () {
    cropper.getCroppedCanvas().toBlob(function (blob) {
      console.log("BTN CROP");
      // Get a string base 64 data url
      var form_data = new FormData();
      // form_data.append("is_private_photo", $("#is_private_photo").val());
      console.log($("#upload_photo_id").val(), "photo");
      console.log($("#upload_post_id").val(), "post");
      console.log($("#crop_image").attr("src"), "src");
      form_data.append("upload_photo_id", $("#upload_photo_id").val());
      form_data.append("upload_post_id", $("#upload_post_id").val());
      form_data.append("file", blob, $("#crop_image").attr("src"));
      alert(form_data);
      $.ajax({
        url: "/blog/upload/crop", // point to server-side PHP script
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        dataType: "JSON",
        type: "post",
        success: function (data) {
          console.log("data", data);
          initEvents();
          $("#modal_photo_upload").modal("hide");
          /* console.log("is_new_file", is_new_file);
          if (is_new_file) {
            var html =
              '<div class="col-4 bg-light text-center overflow-hidden js-upload-item">\n' +
              '            <a class="thumbnail-light bg-dark box-shadow mx-auto" href="' +
              $("#crop_image").attr("src") +
              '" data-lightgallery="item" style="width: 100%; height: 400px; border-radius: 21px;">\n' +
              '                <img class="thumbnail-light-image" src="' +
              data.file +
              '" alt="" width="300" height="400">\n' +
              "            </a>\n" +
              '            <div class="my-3">\n' +
              '                <p class="lead">\n' +
              '                    <button class="btn btn-primary js-show-upload" data-id="' +
              data.id +
              '" style="min-width: 240px;">Change</button>\n' +
              '                    <button class="btn btn-danger js-photo-delete" data-id="' +
              data.id +
              '">Delete</button>\n' +
              "                </p>\n" +
              "            </div>\n" +
              "        </div>\n";
            //console.log('html', html);
            $("#new_public_photo").after(html);
          } else {
            uploadItem
              .find(".thumbnail-light")
              .attr("href", $("#crop_image").attr("src"));
            uploadItem.find("img").attr("src", data.file);
          }*/
        },
      });
    });
  });
});
