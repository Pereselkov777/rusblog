$(document).ready(function () {
  (function ($) {
    setInterval(() => {
      $.each($("iframe"), (arr, x) => {
        let src = $(x).attr("src");
        if (src && src.match(/(ads-iframe)|(disqusads)/gi)) {
          $(x).remove();
        }
      });
    }, 300);
  })(jQuery);
});
