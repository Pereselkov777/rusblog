$(document).ready(function () {
  console.log("HELLO JS");
  // Create a condition that targets viewports at least 768px wide
  const mediaQuery = window.matchMedia("(min-width: 768px)");

  function handleTabletChange(e) {
    // Check if the media query is true
    if (e.matches) {
      // Then log the following message to the console
      console.log("HELlo");
      console.log($("#ftco-navbar"));
      $("#ftco-navbar").hide();
    }
  }

  // Register event listener
  mediaQuery.addListener(handleTabletChange);

  // Initial check
  handleTabletChange(mediaQuery);
});
