-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 03 2021 г., 10:23
-- Версия сервера: 10.3.22-MariaDB
-- Версия PHP: 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('administrator', '8', 1608269472);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('administrator', 1, 'its administrator', NULL, NULL, 1608269418, 1608269418);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `blog_article`
--

CREATE TABLE `blog_article` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `author_id` int(11) DEFAULT NULL COMMENT 'Автор',
  `category_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'Категория',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Изображение',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Алиас',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Заголовок',
  `is_commentable` tinyint(1) NOT NULL COMMENT 'Разрешено комментировать',
  `is_enabled` tinyint(1) NOT NULL COMMENT 'Включено',
  `annotation` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Аннотация',
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Текст',
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Метатеги. Заголовок',
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Метатеги. Ключевые слова',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Метатеги. Описание',
  `created_at` datetime DEFAULT NULL COMMENT 'Создано',
  `updated_at` datetime DEFAULT NULL COMMENT 'Обновлено'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `blog_article`
--

INSERT INTO `blog_article` (`id`, `author_id`, `category_id`, `image`, `slug`, `title`, `is_commentable`, `is_enabled`, `annotation`, `text`, `meta_title`, `keywords`, `description`, `created_at`, `updated_at`) VALUES
(31, 8, 2, 'wPhROnAlJTR_.jpg', 'ddd', 'ddd', 1, 1, 'Инструкция по установке данного сайта', '<p>ghhgh</p>', 'ddd', 'ddd', 'ddd', '2020-12-26 08:47:05', '2020-12-26 08:47:05');

-- --------------------------------------------------------

--
-- Структура таблицы `blog_article_via_tag`
--

CREATE TABLE `blog_article_via_tag` (
  `article_id` int(11) UNSIGNED NOT NULL,
  `tag_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `blog_category`
--

CREATE TABLE `blog_category` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `parent_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'Родитель',
  `slug` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Алиас',
  `title` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Заголовок',
  `created_at` datetime DEFAULT NULL COMMENT 'Создано'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `blog_category`
--

INSERT INTO `blog_category` (`id`, `parent_id`, `slug`, `title`, `created_at`) VALUES
(2, NULL, 'dsds', 'Politics', '2020-12-20 21:49:40');

-- --------------------------------------------------------

--
-- Структура таблицы `blog_tag`
--

CREATE TABLE `blog_tag` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `slug` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Алиас',
  `title` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Заголовок',
  `created_at` datetime DEFAULT NULL COMMENT 'Создано',
  `weight` int(11) DEFAULT NULL COMMENT 'Вес'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `blog_tag`
--

INSERT INTO `blog_tag` (`id`, `slug`, `title`, `created_at`, `weight`) VALUES
(1, 'instrukcia', 'инструкция', '2020-12-17 19:47:04', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `entity` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `entityId` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `level` smallint(6) NOT NULL DEFAULT 1,
  `createdBy` int(11) DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `relatedTo` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `createdAt` int(11) NOT NULL,
  `updatedAt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`id`, `entity`, `entityId`, `content`, `parentId`, `level`, `createdBy`, `updatedBy`, `relatedTo`, `url`, `status`, `createdAt`, `updatedAt`) VALUES
(5, 'f3fbc7b2', 4, 'прпрп', NULL, 1, NULL, NULL, 'app\\modules\\blog\\models\\Post:4', NULL, 1, 1617381087, 1617381087),
(6, 'f3fbc7b2', 4, 'прпрп', NULL, 1, NULL, NULL, 'app\\modules\\blog\\models\\Post:4', NULL, 1, 1617381106, 1617381106),
(7, 'f3fbc7b2', 4, 'sdsds', NULL, 1, NULL, NULL, 'app\\modules\\blog\\models\\Post:4', NULL, 1, 1617381281, 1617381281),
(8, 'f3fbc7b2', 4, 'fdfd', NULL, 1, NULL, NULL, 'app\\modules\\blog\\models\\Post:4', NULL, 1, 1617381575, 1617381575),
(9, 'f3fbc7b2', 4, 'kllklkl', NULL, 1, 8, 8, 'app\\modules\\blog\\models\\Post:4', NULL, 1, 1617428444, 1617428444);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1608259884),
('m010101_100001_init_comment', 1617373228),
('m140209_132017_init', 1608259891),
('m140403_174025_create_account_table', 1608259891),
('m140504_113157_update_tables', 1608259891),
('m140504_130429_create_token_table', 1608259891),
('m140506_102106_rbac_init', 1608259960),
('m140830_171933_fix_ip_field', 1608259891),
('m140830_172703_change_account_table_name', 1608259891),
('m141222_110026_update_ip_field', 1608259891),
('m141222_135246_alter_username_length', 1608259891),
('m150614_103145_update_social_account_table', 1608259891),
('m150623_212711_fix_username_notnull', 1608259891),
('m151218_234654_add_timezone_to_profile', 1608259891),
('m160629_121330_add_relatedTo_column_to_comment', 1617373228),
('m160929_103127_add_last_login_at_to_user_table', 1608259891),
('m161109_092304_rename_comment_table', 1617373228),
('m161114_094902_add_url_column_to_comment_table', 1617373228),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1608259960),
('m180112_103324_add_credit_photo', 1608260056),
('m180201_112357_profile_add_refferal_id', 1608260056),
('m180307_061852_create_blog_tag_table', 1608260077),
('m180307_062100_create_blog_category_table', 1608260077),
('m180307_062116_create_blog_article_table', 1608260078),
('m180307_065349_create_blog_article_via_tag_table', 1608260078),
('m180523_151638_rbac_updates_indexes_without_prefix', 1608259960),
('m180612_014404_add_access_token', 1608260056),
('m180612_014404_add_vk_id', 1608260056),
('m191005_051458_user_add_telegram', 1608260056),
('m200409_110543_rbac_update_mssql_trigger', 1608259960),
('m201228_064254_add_category_column_to_blog_article_table', 1609147045);

-- --------------------------------------------------------

--
-- Структура таблицы `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL COMMENT 'id',
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Текст',
  `user_id` int(11) NOT NULL COMMENT 'id юзера',
  `created_at` datetime NOT NULL COMMENT 'Создано',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Алиас'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `post`
--

INSERT INTO `post` (`id`, `text`, `user_id`, `created_at`, `title`, `slug`) VALUES
(1, 'test', 8, '2021-04-02 11:33:50', 'Тест1', 'test1'),
(2, 'test2', 8, '2021-04-02 11:35:08', 'тест2', 'test2'),
(3, 'Ну ладно это пост. ЗДесь написано про питон', 8, '2021-04-02 15:04:29', 'Тест про питон', 'test-pro-piton'),
(4, 'Текст поста', 8, '2021-04-02 15:06:44', 'Еще один', 'ese-odin');

-- --------------------------------------------------------

--
-- Структура таблицы `post_tag`
--

CREATE TABLE `post_tag` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'id',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Алиас',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Название',
  `weight` int(11) DEFAULT NULL COMMENT 'Вес',
  `created_at` datetime DEFAULT NULL COMMENT 'Создано'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `post_tag`
--

INSERT INTO `post_tag` (`id`, `slug`, `title`, `weight`, `created_at`) VALUES
(1, 'test1', 'тест1', 1, '2021-04-02 11:33:50'),
(2, 'test2', 'тест2', 1, '2021-04-02 11:35:08'),
(3, 'test3', 'test3', 1, '2021-04-02 14:08:20'),
(4, 'test4', 'test4', 2, '2021-04-02 14:21:36'),
(5, 'piton', 'питон', 2, '2021-04-02 15:04:29'),
(6, 'programmirovanie', 'программирование', 1, '2021-04-02 15:04:29'),
(7, 'veb', 'веб', 1, '2021-04-02 15:04:29'),
(8, 'bazy-dannyh', 'базы данных', 1, '2021-04-02 15:06:44');

-- --------------------------------------------------------

--
-- Структура таблицы `post_via_tag`
--

CREATE TABLE `post_via_tag` (
  `post_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `post_via_tag`
--

INSERT INTO `post_via_tag` (`post_id`, `tag_id`) VALUES
(1, 1),
(1, 4),
(2, 2),
(2, 3),
(2, 4),
(3, 5),
(3, 6),
(3, 7),
(4, 5),
(4, 8);

-- --------------------------------------------------------

--
-- Структура таблицы `profile`
--

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit` decimal(10,2) NOT NULL DEFAULT 0.00,
  `referrer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`, `timezone`, `photo`, `credit`, `referrer_id`) VALUES
(8, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', NULL),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `social_account`
--

CREATE TABLE `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `token`
--

CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `token`
--

INSERT INTO `token` (`user_id`, `code`, `created_at`, `type`) VALUES
(9, 'wWNJytTy0XGVqZnr4Lc-x3XQ5yFNbaPb', 1617379284, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT 0,
  `last_login_at` int(11) DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vk_id` int(11) DEFAULT NULL,
  `vk_auth_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel_username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel_id` bigint(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`, `access_token`, `vk_id`, `vk_auth_key`, `tel_username`, `tel_id`) VALUES
(8, 'admin', 'aleksandrpereselkov77@gmail.com', '$2y$10$WO89qbglwzPnPamLCCFOKOSOvkVnNA3tfEzZkqvJXOrvYjvKIOLee', 'rnQdvP1YJAFO4QaPGsZTpkv2xmj-dymf', 1608269273, NULL, NULL, '127.0.0.1', 1608269250, 1608269250, 0, 1617421263, '5fdc3dc298311', NULL, NULL, NULL, NULL),
(9, 'rus', 'ruslanperesy@gmail.com', '$2y$10$kzRgGeP0UFPv7xx4IxZWa.Fggwnwf/HQi9UM2dMMwdvu0asHjP6vS', '877QFvNGtdgryjbkuKqXsQEogCFVE1H-', NULL, NULL, NULL, '127.0.0.1', 1617379284, 1617379284, 0, NULL, '60673fd4c3cbc', NULL, NULL, NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `blog_article`
--
ALTER TABLE `blog_article`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `idx_blog_article_category_id` (`category_id`),
  ADD KEY `idx_blog_article_author_id` (`author_id`),
  ADD KEY `idx_blog_article_created_at` (`created_at`);

--
-- Индексы таблицы `blog_article_via_tag`
--
ALTER TABLE `blog_article_via_tag`
  ADD PRIMARY KEY (`article_id`,`tag_id`),
  ADD KEY `idx_article_id` (`article_id`),
  ADD KEY `idx_tag_id` (`tag_id`);

--
-- Индексы таблицы `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD UNIQUE KEY `parent_id` (`parent_id`);

--
-- Индексы таблицы `blog_tag`
--
ALTER TABLE `blog_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD UNIQUE KEY `title` (`title`),
  ADD KEY `idx_blog_tag_weight` (`weight`);

--
-- Индексы таблицы `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-Comment-entity` (`entity`),
  ADD KEY `idx-Comment-status` (`status`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `title` (`title`),
  ADD KEY `slug` (`slug`);

--
-- Индексы таблицы `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slug` (`slug`),
  ADD KEY `Weight` (`weight`);

--
-- Индексы таблицы `post_via_tag`
--
ALTER TABLE `post_via_tag`
  ADD PRIMARY KEY (`post_id`,`tag_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `tag_id` (`tag_id`);

--
-- Индексы таблицы `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_unique` (`provider`,`client_id`),
  ADD UNIQUE KEY `account_unique_code` (`code`),
  ADD KEY `fk_user_account` (`user_id`);

--
-- Индексы таблицы `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_username` (`username`),
  ADD UNIQUE KEY `user_unique_email` (`email`),
  ADD KEY `idx_vk_id` (`vk_id`),
  ADD KEY `idx-user-tel_id` (`tel_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `blog_article`
--
ALTER TABLE `blog_article`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT для таблицы `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `blog_tag`
--
ALTER TABLE `blog_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `blog_article_via_tag`
--
ALTER TABLE `blog_article_via_tag`
  ADD CONSTRAINT `fg_blog_article_via_tag_article_id` FOREIGN KEY (`article_id`) REFERENCES `blog_article` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fg_blog_article_via_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `blog_tag` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
