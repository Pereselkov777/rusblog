<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%post}}`.
 */
class m210403_160913_add_photo_full_column_to_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'photo_full', $this->string()->null());
        $this->addColumn('{{%profile}}', 'photo_prev', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile}}', 'photo_full');
        $this->dropColumn('{{%profile}}', 'photo_prev');
    }
}
